import pygame
import random
import math
    

#muzika
pygame.init()

pygame.mixer.music.load("Muzika.mp3")


white=(215,215,215)
black=(0,0,0)
red=(255,0,0)
blue=(0,0,255)
green=(0,128,0)
pygame.init()
myfont = pygame.font.SysFont("monospace", 20)
sirina_ekrana=1280
visina_ekrana=800
ekran = pygame.display.set_mode((sirina_ekrana, visina_ekrana))
pygame.display.set_caption('Multitask game')
clock = pygame.time.Clock()


class Neprijatelj():
    def __init__(self,x,y,pomeraj,brzina=1,r=5):
        self.x=x
        self.y=y
        self.pomeraj=pomeraj
        self.brzina=brzina
        self.r=r
        self.vreme=9
    def pomeri(self):
        self.x+=self.pomeraj[0]
        self.y+=self.pomeraj[1]

class Igrac():
    def __init__(self,x,y,r=20):
        self.x=x
        self.y=y
        self.pomeraj=[0,0]
        self.r=r

    
    def pomeri_igra3(self):
        if self.pomeraj[1]<0:
            if self.y  > koordinate_d_d[1]+self.r:
                self.y += self.pomeraj[1]
        else:
            if self.y < koordinate_d_d[3]-self.r:
                self.y+=self.pomeraj[1]
        if self.pomeraj[0]<0:
            if self.x  > koordinate_d_d[0]+self.r :
                self.x += self.pomeraj[0]
        else:
            if self.x < koordinate_d_d[2]-self.r:
                self.x+=self.pomeraj[0]
    
    def pomeri_igra4(self):
        if self.pomeraj[1]<0:
            if self.y  > koordinate_d_l[1]+self.r:
                self.y += self.pomeraj[1]
        else:
            if self.y < koordinate_d_l[3]-self.r:
                self.y+=self.pomeraj[1]
    
    def pomeri_igra1(self):
        if self.pomeraj[0]<0:
            if self.x  > 0+self.r :
                self.x += self.pomeraj[0]
        else:
            if self.x < sirina_ekrana/2-2-self.r:
                self.x+=self.pomeraj[0]

slika_igraca_prve_igre=pygame.image.load("korpica.png")
slika_jabuke=pygame.image.load("jabuka.png")
slika_broda=pygame.image.load("brodic.png")
slika_kugle=pygame.image.load("kugla.png")
slika_metka=pygame.image.load("metak.png")
slika_zida=pygame.image.load("zid.png")
slika_igraca3=pygame.image.load("igrac_3.png")

koordinate_d_d = (sirina_ekrana / 2 + 2, visina_ekrana / 2 + 2, sirina_ekrana, visina_ekrana)
koordinate_d_l = (0, visina_ekrana / 2 + 2, sirina_ekrana / 2 - 2, visina_ekrana)

def nacrtajPodeluEkrana(ekran):
    pygame.draw.line(ekran, black, (sirina_ekrana/2-2,0), (sirina_ekrana/2-2,visina_ekrana), 5)
    pygame.draw.line(ekran, black, (0, visina_ekrana/2-2), (sirina_ekrana, visina_ekrana/2-2), 5)

def napraviMogucePozicijePrveIgre():
    koordinate_g_l = (0, 0, sirina_ekrana / 2 - 2, visina_ekrana / 2 - 2)
    povratna_lista = []
    for i in range(5):
        x=(int(koordinate_g_l[2]*3/10)+int((koordinate_g_l[2]/10)*i))
        y=int(koordinate_g_l[3]/2)
        povratna_lista.append((x,y))
    return  povratna_lista

def napraviMogucePozicijeDrugeIgre():
    koordinate_g_d=(sirina_ekrana/2+2,0,sirina_ekrana,visina_ekrana/2-2)
    povratna_lista=[]
    for i in range(5):
        x=(int((koordinate_g_d[2]+koordinate_g_d[0])/2))
        y=(int(koordinate_g_d[3]*3/10)+int((koordinate_g_d[3]/10)*i))
        povratna_lista.append((x,y))
    return povratna_lista

def nacrtajIgracaPrveIgre(igrac):
    ekran.blit(slika_igraca_prve_igre,(igrac.x-igrac.r,igrac.y-igrac.r))

def nacrtajIgracaCetvrteIgre(igrac):
    ekran.blit(slika_metka, (igrac.x - igrac.r, igrac.y - igrac.r))

def napraviRandomNeprijateljaPrvaIgra(lista_nep):
    koordinate_g_l = (0, 0, sirina_ekrana / 2 - 2, visina_ekrana / 2 - 2)
    lista_mog=[]
    for i in range(8):
        pomeraj_g=(0,1)
        x=(int(koordinate_g_l[2]/10)+int((koordinate_g_l[2]/10)*i))
        y_g= 0
        nep_g=Neprijatelj(x,y_g,pomeraj_g,1,10)
        lista_mog.append(nep_g)
    lista_nep.append(random.choice(lista_mog))
    return lista_nep

def napraviRandomNeprijateljaDrugaIgra(lista_nep):
    koordinate_g_d = (sirina_ekrana / 2 + 2, 0, sirina_ekrana, visina_ekrana / 2 - 2)
    lista_mog=[]
    for i in range(5):
        pomeraj_g=(1,0)
        pomeraj_d=(-1,0)
        x_g=int(koordinate_g_d[0])
        x_d=int(koordinate_g_d[2])
        y=(int(koordinate_g_d[3]*3/10)+int((koordinate_g_d[3]/10)*i))
        nep_g=Neprijatelj(x_g,y,pomeraj_g,1,10)
        nep_d=Neprijatelj(x_d, y, pomeraj_d,1,10)
        lista_mog.append(nep_g)
        lista_mog.append(nep_d)
    lista_nep.append(random.choice(lista_mog))
    return lista_nep

def napraviRandomNeprijateljaTrecaIgra(lista_nep):
    x=random.randint(koordinate_d_d[0]+10, koordinate_d_d[2]-10)
    y = random.randint(koordinate_d_d[1]+10, koordinate_d_d[3]-10)
    nep=Neprijatelj(x,y,(0,0),1,10)
    lista_nep.append(nep)
    return lista_nep

def napraviRanodmNeprijateljaCetvrtaIgra(lista_nep):
    opcija=random.randint(0,2)
    if opcija==0:
        for i in range(1,7):
            x=int(koordinate_d_l[2]-15)
            y=int(koordinate_d_l[1]-10+i*20)
            nep=Neprijatelj(x,y,(-1,0),1,10)
            lista_nep.append(nep)
    elif opcija==1:
        for i in range(7,14):
            x=int(koordinate_d_l[2]-15)
            y=int(koordinate_d_l[1]-10+i*20)
            nep=Neprijatelj(x,y,(-1,0),1,10)
            lista_nep.append(nep)
    elif opcija==2:
        for i in range(14,21):
            x=int(koordinate_d_l[2]-15)
            y=int(koordinate_d_l[1]-10+i*20)
            nep=Neprijatelj(x,y,(-1,0),1,10)
            lista_nep.append(nep)
    return lista_nep

def NacrtajNeprijatelje(lista_nep):
    for nep in lista_nep:
        ekran.blit(slika_kugle,(nep.x-nep.r,nep.y-nep.r))
        #pygame.draw.circle(ekran, red, (nep.x,nep.y), nep.r)

def NacrtajZid(lista_zid):
    for nep in lista_zid:
        ekran.blit(slika_zida,(nep.x-nep.r,nep.y-nep.r))

def NacrtajSkupljace(lista_skup):
    for nep in lista_skup:
        pygame.draw.circle(ekran, (0,28*nep.vreme,255-28*nep.vreme), (nep.x,nep.y), nep.r)

def NacrtajJabuke(lista_jabuka):
    for jabuka in lista_jabuka:
        ekran.blit(slika_jabuke, (jabuka.x - jabuka.r, jabuka.y - jabuka.r))

def nacrtajIgruDruguIgru(ekran, moguce_pozicije_druga_igra, indeks_igraca_druga_igra):
    indeks=0
    for mog in moguce_pozicije_druga_igra:
        if indeks!=indeks_igraca_druga_igra:
            pygame.draw.circle(ekran, blue, (mog[0], mog[1]),5,1)
        else:
            ekran.blit(slika_broda,(mog[0]-20,mog[1]-20))
            #pygame.draw.circle(ekran, blue, (mog[0], mog[1]), 10)
        indeks+=1

def PomerajNeprijatelje(lista_nep):
    for nep in lista_nep:
        nep.pomeri()

def IzbaciNeprijatelje(lista_nep):
    koordinate_g_l = (0, 0, sirina_ekrana / 2 - 2, visina_ekrana / 2 - 2)
    izlaz_lista=lista_nep
    for nep in lista_nep:
        if nep.y>koordinate_g_l[3]-10 or nep.y<koordinate_g_l[0]:
            izlaz_lista.remove(nep)
            return izlaz_lista, True
    return izlaz_lista,False

def IzbaciNeprijatelje2(lista_nep,skor):
    koordinate_g_d = (sirina_ekrana / 2 + 2, 0, sirina_ekrana, visina_ekrana / 2 - 2)
    izlaz_lista = lista_nep
    for nep in lista_nep:
        if nep.x > koordinate_g_d[2] or nep.x < koordinate_g_d[0]:
            skor += 50
            izlaz_lista.remove(nep)
    return izlaz_lista, skor

def IzbaciNeprijatelje3(lista_nep,skor):
    for nep in lista_nep:
        if nep.x<0:
            lista_nep.remove(nep)
            skor+=15
    return lista_nep,skor

def ProveraSudara(lista_nep,lista_mesta,indeks):
    for nep in lista_nep:
        udaljenost_centrova=math.sqrt(math.pow(nep.x-lista_mesta[indeks][0],2)+math.pow(nep.y-lista_mesta[indeks][1],2))
        if nep.r+10>udaljenost_centrova:
            return True

def nacrtajSkor(ekran,skor):
    label = myfont.render("SCORE:"+str(skor), 1, (0, 0, 0))
    r=label.get_rect()
    r=((sirina_ekrana/2-label.get_width()/2)-10,0,label.get_width()+20,label.get_height())
    pygame.draw.rect(ekran, white, r)
    pygame.draw.rect(ekran,black,r,3)
    ekran.blit(label, (sirina_ekrana/2-label.get_width()/2, 0))

def nacrtajHSkor(ekran,hskor):
    label = myfont.render("HIGHSCORE:"+str(hskor), 1, (0, 0, 0))
    r=label.get_rect()
    r=((sirina_ekrana/2-label.get_width()/2)-10,visina_ekrana-label.get_height(),label.get_width()+20,label.get_height())
    pygame.draw.rect(ekran, white, r)
    pygame.draw.rect(ekran,black,r,3)
    ekran.blit(label, (sirina_ekrana/2-label.get_width()/2,visina_ekrana-label.get_height()))

def NacrtajIgracaIgraTri(igrac):
    ekran.blit(slika_igraca3,(igrac.x-igrac.r,igrac.y-igrac.r))
    #pygame.draw.circle(ekran, blue, (igrac.x,igrac.y), igrac.r)

def ProveraSudaraTrecaIgra(lista_nep,igrac,skor):
    for nep in lista_nep:
        udaljenost_centrova=math.sqrt(math.pow(nep.x-igrac.x,2)+math.pow(nep.y-igrac.y,2))
        if nep.r+igrac.r>udaljenost_centrova:
            lista_nep.remove(nep)
            skor+=100
    return skor

def ProveraSudaraCetvrtaIgra(lista_nep,igrac):
    for nep in lista_nep:
        udaljenost_centrova=math.sqrt(math.pow(nep.x-igrac.x,2)+math.pow(nep.y-igrac.y,2))
        if nep.r+igrac.r>udaljenost_centrova:
            return True
    return False
    
def igra():
    pygame.mixer.music.play(-1)

    f = open("highscore.txt")
    lines = f.read().splitlines()
    if(len(lines)!=0):
        broj = lines[0]
    else:
        broj = 0
    f.close()
    HIGH_SCORE = int(broj)

    
    kraj=False
    moguce_pozicije_prva_igra=napraviMogucePozicijePrveIgre()
    moguce_pozicije_druga_igra=napraviMogucePozicijeDrugeIgre()
    indeks_igraca_druga_igra=2
    indeks_igraca_prva_igra=2
    lista_neprijatelja_prva_igra=[]
    lista_neprijatelja_druga_igra=[]
    lista_neprijatelja_treca_igra=[]
    lista_neprijatelja_cetvrta_igra=[]
    igrac_igre_jedan=Igrac(int((sirina_ekrana/2-2)/2),int(visina_ekrana/2-2)-40,20)
    igrac_igra_tri=Igrac(int((koordinate_d_d[2]+koordinate_d_d[0])/2),int((koordinate_d_d[3]+koordinate_d_d[1])/2))
    igrac_igre_cetiri=Igrac(koordinate_d_l[0]+60,koordinate_d_l[3]-20,10)
    broj_frejmova=0
    brzina_pojavljivanja=360
    brzina_pojavljivanja2=180
    brzina_pojavljivanja3=180
    brzina_pojavljivanja4=240
    skor_drugi_nivo=500
    skor_treci_nivo=1500
    skor_cetvrti_nivo=3000
    skor=0

    while not kraj:
        
        broj_frejmova+=1
        
        #inicijalizacija ekrana
        ekran.fill(white)
        nacrtajPodeluEkrana(ekran)
        nacrtajHSkor(ekran,HIGH_SCORE)
        nacrtajSkor(ekran, skor)

        if broj_frejmova%brzina_pojavljivanja==0:
            lista_neprijatelja_prva_igra=napraviRandomNeprijateljaPrvaIgra(lista_neprijatelja_prva_igra)
        lista_neprijatelja_prva_igra,kraj=IzbaciNeprijatelje(lista_neprijatelja_prva_igra)
        skor=ProveraSudaraTrecaIgra(lista_neprijatelja_prva_igra,igrac_igre_jedan,skor)
        igrac_igre_jedan.pomeri_igra1()
        if kraj==True:
            if skor>HIGH_SCORE:
                    upis = open("highscore.txt","w")
                    upis.write(str(skor))
                    upis.close()
            break
        nacrtajIgracaPrveIgre(igrac_igre_jedan)
        NacrtajJabuke(lista_neprijatelja_prva_igra)
        PomerajNeprijatelje(lista_neprijatelja_prva_igra)
        
        #druga igra
        if skor>=skor_drugi_nivo:
            nacrtajIgruDruguIgru(ekran, moguce_pozicije_druga_igra, indeks_igraca_druga_igra)#crtanje druge igre
            if broj_frejmova%brzina_pojavljivanja2==0:
                lista_neprijatelja_druga_igra=napraviRandomNeprijateljaDrugaIgra(lista_neprijatelja_druga_igra)
            NacrtajNeprijatelje(lista_neprijatelja_druga_igra)
            PomerajNeprijatelje(lista_neprijatelja_druga_igra)
            lista_neprijatelja_druga_igra,skor=IzbaciNeprijatelje2(lista_neprijatelja_druga_igra,skor)
            kraj = ProveraSudara(lista_neprijatelja_druga_igra, moguce_pozicije_druga_igra, indeks_igraca_druga_igra)
            if kraj==True:
                if skor>HIGH_SCORE:
                    upis = open("highscore.txt","w")
                    upis.write(str(skor))
                    upis.close()
                break
        
        #treca igra
        if skor>=skor_treci_nivo:
            if broj_frejmova % 60 == 0:
                for nep in lista_neprijatelja_treca_igra:
                    if nep.vreme == 1:
                        kraj = True
                        if skor>HIGH_SCORE:
                            upis = open("highscore.txt","w")
                            upis.write(str(skor))
                            upis.close()
                        break
                    nep.vreme -= 1
            if broj_frejmova%brzina_pojavljivanja3==0:
                lista_neprijatelja_treca_igra = napraviRandomNeprijateljaTrecaIgra(lista_neprijatelja_treca_igra)
            NacrtajIgracaIgraTri(igrac_igra_tri)
            skor=ProveraSudaraTrecaIgra(lista_neprijatelja_treca_igra,igrac_igra_tri,skor)
            igrac_igra_tri.pomeri_igra3()
            NacrtajSkupljace(lista_neprijatelja_treca_igra)
        #cetvrta igra
        
        if skor>skor_cetvrti_nivo:
            nacrtajIgracaCetvrteIgre(igrac_igre_cetiri)
            igrac_igre_cetiri.pomeri_igra4()
            PomerajNeprijatelje(lista_neprijatelja_cetvrta_igra)
            lista_neprijatelja_cetvrta_igra,skor=IzbaciNeprijatelje3(lista_neprijatelja_cetvrta_igra,skor)
            if broj_frejmova%brzina_pojavljivanja4==0:
                lista_neprijatelja_cetvrta_igra=napraviRanodmNeprijateljaCetvrtaIgra(lista_neprijatelja_cetvrta_igra)
            if ProveraSudaraCetvrtaIgra(lista_neprijatelja_cetvrta_igra,igrac_igre_cetiri):
                kraj=True
                if skor>HIGH_SCORE:
                    upis = open("highscore.txt","w")
                    upis.write(str(skor))
                    upis.close()
                break
            NacrtajZid(lista_neprijatelja_cetvrta_igra)
        
        if skor>3000 and skor%1000==0:
            if brzina_pojavljivanja>180:
                brzina_pojavljivanja -= 10
            if brzina_pojavljivanja2>90:
                brzina_pojavljivanja2 -= 5
            if brzina_pojavljivanja3>90:
                brzina_pojavljivanja3-=5
            if brzina_pojavljivanja4>120:
                brzina_pojavljivanja4 -=10
        
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                kraj = True
            elif event.type == pygame.KEYDOWN:
                if event.key==pygame.K_LEFT:
                   igrac_igre_jedan.pomeraj[0]=-3
                if event.key==pygame.K_RIGHT:
                    igrac_igre_jedan.pomeraj[0] = 3
                if skor>=skor_drugi_nivo:
                    if event.key==pygame.K_UP:
                        if indeks_igraca_druga_igra<=4 and indeks_igraca_druga_igra>0:
                            indeks_igraca_druga_igra-=1
                    if event.key==pygame.K_DOWN:
                        if indeks_igraca_druga_igra<4 and indeks_igraca_druga_igra>=0:
                            indeks_igraca_druga_igra+=1
                if skor>=skor_treci_nivo:
                    if event.key==pygame.K_w:
                        igrac_igra_tri.pomeraj[1]=-3
                    if event.key==pygame.K_a:
                        igrac_igra_tri.pomeraj[0]=-3
                    if event.key==pygame.K_s:
                        igrac_igra_tri.pomeraj[1]=3
                    if event.key==pygame.K_d:
                        igrac_igra_tri.pomeraj[0]=3
                if skor>=skor_cetvrti_nivo:
                    if event.key==pygame.K_SPACE:
                        igrac_igre_cetiri.pomeraj[1]=-3

            elif event.type==pygame.KEYUP:
                if event.key==pygame.K_LEFT:
                    if igrac_igre_jedan.pomeraj[0]<0:
                        igrac_igre_jedan.pomeraj[0]=0
                if event.key==pygame.K_RIGHT:
                    if igrac_igre_jedan.pomeraj[0]>0:
                        igrac_igre_jedan.pomeraj[0] = 0
                if skor>=skor_treci_nivo:
                    if event.key==pygame.K_w:
                        if igrac_igra_tri.pomeraj[1]<0:
                            igrac_igra_tri.pomeraj[1]=0
                    if event.key==pygame.K_a:
                        if igrac_igra_tri.pomeraj[0]<0:
                            igrac_igra_tri.pomeraj[0]=0
                    if event.key==pygame.K_s:
                        if igrac_igra_tri.pomeraj[1]>0:
                            igrac_igra_tri.pomeraj[1]=0
                    if event.key==pygame.K_d:
                        if igrac_igra_tri.pomeraj[0] > 0:
                            igrac_igra_tri.pomeraj[0]=0
                if skor>=skor_cetvrti_nivo:
                    if event.key==pygame.K_SPACE:
                        igrac_igre_cetiri.pomeraj[1]=3

        
        pygame.display.flip()
        clock.tick(60)
def pocetniEkran():
    kraj=False
    font2 = pygame.font.SysFont("comicsansms", 40)
    label = font2.render("TEST YOUR MULTITASKING SKILL!", 1, (0, 0, 0))
    label1= font2.render("1.COLLECT APPLES using LEFT, RIGHT arrows",1,(0,0,0))
    label2=font2.render("2.DODGE CANNONBALLS using UP, DOWN arrows",1,(0,0,0))
    label3=font2.render("3.COLLECT ALIENS using W, A, S, D keys",1,black)
    label4=font2.render("4.DODGE WALLS using SPACE key",1,black)
    label5=font2.render("Press [ENTER] to start!",1,black)

    lista_labeli=[label1,label2,label3,label4]
    
    while not kraj:

        ekran.fill(white)
        i=0
        ekran.blit(label, (sirina_ekrana / 2 - label.get_width() / 2, 0))
        for labela in lista_labeli:
            ekran.blit(labela, (sirina_ekrana / 2 - labela.get_width() / 2, 150+50*i))
            i+=1
        ekran.blit(label5, (sirina_ekrana / 2 - label5.get_width() / 2, 550))
        #nacrtajPodeluEkrana(ekran)
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return True
            elif event.type == pygame.KEYDOWN:
                if event.key==pygame.K_RETURN:
                    return False
                if event.key==pygame.K_ESCAPE:
                    return True
kraj=pocetniEkran()
while not kraj:
    igra()
    kraj = pocetniEkran()
