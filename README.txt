MULTITASKING GAME

Da bi se projekat pokrenuo potrebna je python verzija 2.7 ili više zajedno sa paketom pygame koji 
se može lako instalirati pomoću synaptic package manager-a.

Program radi na svakom racunaru koji ima instaliran gore navedeni softver, 
pošto se python interpretira(nije potrebno nikakvo bildovanje ili kompilacija),
porgram pokrećemo iz konzole komandom `python mtgame.py`.

Poenta igre je izdržati što duže, i obavljati više zadataka u isto vreme.

Na strelicu desno i strelicu levo pomeramo korpu i skupljamo jabuke, igrač ne sme propustiti ni jednu
Na strelice gore i dole pomeramo brod i izbegavamo topovsku đulad
Na W,A,S,D igrač treba da pomera svemirski brod i sakupi zelene tačke
Na SPACE izbegavamo zidove tako što menjamo smer gravitacije 

Autori:
	Vladimir Đošović vladimirdjosowic@gmail.com 
	David Marjanović david.marjanovic995@gmail.com
